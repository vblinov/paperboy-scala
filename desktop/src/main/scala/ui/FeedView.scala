package ui

import scala.swing.ListView
import scala.concurrent.{ExecutionContext, Future}
import paperboy.rss.{Item, Feed}
import scala.util.{Try, Success}
import ExecutionContext.Implicits.global
import scala.swing.ListView.{Renderer, IntervalMode}

class FeedView extends ListView[Item] {
    selection.intervalMode = IntervalMode.Single
    renderer = Renderer((item:Item) => (item.title, item.author) match {
        case (Some(title), Some(author)) => s"$title by $author"
        case (Some(title), _) => title
        case _ => "article"
    })

    def setFeed(feed: Future[Feed]) = {
        feed onComplete tryToDisplayFeed
    }

    private def tryToDisplayFeed(loadedFeed: Try[Feed]) = loadedFeed match {
        case Success(feed) => {
            listData = for (
                item <- feed.channel.items
            ) yield item
            println("List data is setted up")
        }
        case _ => println("Feed loading failure")
    }
}
