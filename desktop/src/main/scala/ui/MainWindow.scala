package ui

import paperboy.FeedLoader
import scala.swing._
import scala.swing.event.ListSelectionChanged
import scala.swing.GridBagPanel.Fill
import scala.swing.ScrollPane.BarPolicy
import javax.swing.event.{HyperlinkEvent, HyperlinkListener}
import java.net.URL
import java.awt.{Cursor, Desktop}
import scala.Predef.String

class MainWindow(feedUrl: String) extends MainFrame {
    val articleContentView = new EditorPane
    articleContentView.contentType = "text/html"
    articleContentView.editable = false
    articleContentView.peer.addHyperlinkListener(new HyperlinkListener() {
        def hyperlinkUpdate(hyperlinkEvent:HyperlinkEvent) = hyperlinkEvent.getEventType match {
            case HyperlinkEvent.EventType.ENTERED => articleContentView.peer.setCursor(new Cursor(Cursor.HAND_CURSOR))
            case HyperlinkEvent.EventType.EXITED => articleContentView.peer.setCursor(null)
            case HyperlinkEvent.EventType.ACTIVATED => openLink(hyperlinkEvent.getURL)
        }
    })


    val feedView = new FeedView

    listenTo(feedView.selection)
    reactions += {
        case ListSelectionChanged(`feedView`, range, live) =>
            articleContentView.text = feedView.listData(range(0)).description match {
                case Some(text) => "<html>" + text + "</html>"
                case _ => "no article selected"
            }
    }

    feedView setFeed FeedLoader.getFeed(feedUrl)

    val feedViewerPanel = new GridBagPanel {
        val constraint = new Constraints()
        constraint.fill = Fill.Both

        constraint.grid = (0, 0)
        constraint.weightx = 1.0
        constraint.weighty = 1.0

        add(new ScrollPane(feedView) { horizontalScrollBarPolicy = BarPolicy.Never }, constraint )

        constraint.grid = (0, 1)
        constraint.ipady = 5
        add(new ScrollPane(articleContentView) { horizontalScrollBarPolicy = BarPolicy.Never }, constraint )
    }

    peer setContentPane feedViewerPanel.peer

    size = new Dimension(800, 600)
    centerOnScreen()

    def openLink(link: URL) = {
        if (Desktop.isDesktopSupported) {
            try {
                val desktop = Desktop.getDesktop
                link.getProtocol match {
                    case "mailto" => desktop.mail(link.toURI)
                    case scheme:String if "http" :: "http" :: Nil contains scheme => desktop.browse(link.toURI)
                    case x => System.err.println("Can't open link " + x)
                }
            } catch {
                case x: Throwable => System.err.println("Can't open link due to error " + x)
            }
        }
    }
}
