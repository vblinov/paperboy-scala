import paperboy.FeedLoader
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.swing.SimpleSwingApplication
import ui.MainWindow

object Main extends SimpleSwingApplication {
    val habraFeed = "http://habrahabr.ru/rss/hub/scala/"

    def top = {
      val window = new MainWindow(habraFeed)
      window.title = "Paperboy RSS reader: Habrahabr / Scala"
      window
    }

    def testRun() {
        val feedResponse = FeedLoader.getFeed(habraFeed)
        val feed = Await.result(feedResponse, 60 seconds)

        for (
            item <- feed.channel.items
        ) {
            println(s"${item.title} by ${item.author}")
            println("------------")
            println(s"${item.description}")
            println("")
        }
    }
}
