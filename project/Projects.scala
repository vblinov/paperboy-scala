import sbt._
import sbt.Keys._

object Projects {
    def AndroidProject(name: String, dir: String) = {
        CommonProject(name, dir, Dependencies.androidDependencies).settings(Settings.androidSettings:_*)
    }

    def DesktopProject(name: String, dir: String) = {
        CommonProject(name, dir, Dependencies.desktopDependencies)
    }

    def CoreProject(name: String, dir: String) = {
        CommonProject(name, dir, Dependencies.coreDependencies)
    }


    def CommonProject(name: String, dir: String, deps: Seq[ModuleID]):Project = {
        val project = Project(name, file(dir))
        project.settings(Settings.commonSettings ++ Seq(
            libraryDependencies ++= deps
        ): _*)
    }
}
