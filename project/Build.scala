import sbt.Keys._
import sbt._
import android.Keys._

object PaperboyBuild extends Build {
    lazy val root = Project(id = "paperboy", base = file(".")).settings(Seq(
        scalaVersion     := "2.11.1",
        packageRelease   <<= packageRelease in Android in `android-app`,
        packageDebug     <<= packageDebug in Android in `android-app`,
        run              <<= run in Android in `android-app`
    ) ++ android.Plugin.androidCommands: _*).aggregate(core, `android-app`, `desktop-app`)


    lazy val core = Projects.CoreProject("core", "core")
    lazy val `android-app` = Projects.AndroidProject("android", "android").dependsOn(core)
    lazy val `desktop-app` = Projects.DesktopProject("desktop", "desktop").dependsOn(core)
}
