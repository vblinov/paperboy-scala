import android.Keys._
import sbt.Keys._
import sbt._

object Settings {
    lazy val commonSettings = Seq(
        organization     := "com.github.dant3",
        scalaVersion     := "2.11.1",
        version          := "1.0",
        javacOptions     ++= Seq("-source", "1.7", "-target", "1.7"),
        scalacOptions    += "-target:jvm-1.7",
        exportJars       := true
    )


    lazy val androidSettings = android.Plugin.androidBuild ++ Seq(
        platformTarget  in Android  := "android-L",
        proguardScala   in Android  := true,
        run                         <<= run in Android,
        proguardOptions in Android  ++= Seq(
            "-ignorewarnings",
            "-keep class scala.Dynamic",
            "-keep class paperboy.** { *; }"
        ),
        apkbuildExcludes in Android ++= Seq(
            "META-INF/LICENSE.txt",
            "META-INF/NOTICE.txt"
        ),

        resolvers ++= Seq(
            Resolver.sonatypeRepo("releases"),
            "jcenter" at "http://jcenter.bintray.com"
        ),

        scalacOptions in (Compile, compile) ++=
            (dependencyClasspath in Compile).value.files.map("-P:wartremover:cp:" + _.toURI.toURL),
        scalacOptions in (Compile, compile) ++= Seq(
            "-P:wartremover:traverser:macroid.warts.CheckUi"
        ),

        libraryDependencies += Dependencies.wartRemoverPlugin,

        collectResources in Android := {
            Tasks.collectGeneratedBuildInfo.value
            (collectResources in Android).value
        }
    ) ++ android.buildinfo.Plugin.androidBuildInfoSettings
}
