import android.Dependencies._
import sbt._

object Dependencies {
    val scalaTest = "org.scalatest" %% "scalatest" % "2.2.0"
    val dispatch = "net.databinder.dispatch" %% "dispatch-core" % "0.11.0"
    val logbackClassic = "ch.qos.logback" % "logback-classic" % "1.0.13"

    // desktop
    val scalaSwing = "org.scala-lang" % "scala-swing" % "2.10.2"

    // Rx.java
    val rxJavaVersion = "0.20.0-RC4"
    val rxJavaLibs = Seq(
        "com.netflix.rxjava" % "rxjava-scala",
        "com.netflix.rxjava" % "rxjava-android"
    ).map({ _ % rxJavaVersion })

    // android + scala
    val macroid = aar("org.macroid" %% "macroid" % "2.0.0-M3")
    val wartRemoverPlugin = compilerPlugin("org.brianmckenna" %% "wartremover" % "0.10")
    val scaloid = "org.scaloid" %% "scaloid" % "3.4-10"


    // android
    val googlePlayServices = aar("com.google.android.gms" % "play-services" % "4.0.30")
    val androidSupport = aar("com.android.support" % "support-v4" % "20.0.0")
    val fab = aar("com.shamanland" % "fab" % "0.0.5")
    val picasso =   "com.squareup.picasso" % "picasso" % "2.3.4"


    // common & usefull
    val scalaRx = "com.scalarx" %% "scalarx" % "0.2.5"
    val scalaTime = "com.github.nscala-time" %% "nscala-time" % "1.4.0"
    val reactiveCollections = "com.storm-enroute" %% "reactive-collections" % "0.5"


    val testFrameworks = Seq(
        scalaTest % "test"
    )


    val coreDependencies = Seq(
        logbackClassic, dispatch, scalaTime
    ) ++ testFrameworks

    val desktopDependencies = Seq(
        scalaSwing
    ) ++ testFrameworks

    val androidDependencies = Seq(
        scalaRx, macroid, scaloid,
        fab, picasso, reactiveCollections
    )
}