/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import sbt._
import android.Keys._

object Tasks {
    def collectGeneratedBuildInfo = (android.buildinfo.Plugin.androidBuildInfo,
                                     projectLayout in Android).map {
        (buildInfo, layout) => {
            val resTarget = layout.bin / "resources" / "res" / "values"
            for {
                file <- buildInfo
            } IO.copyFile(file, resTarget / file.getName, preserveLastModified = true)
        }
    }
}