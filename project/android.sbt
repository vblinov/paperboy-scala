addSbtPlugin("com.hanhuy.sbt" % "android-sdk-plugin" % "1.3.11")

resolvers += Resolver.sbtPluginRepo("snapshots")

addSbtPlugin("com.hanhuy.sbt" % "sbt-idea" % "1.7.0-SNAPSHOT")

resolvers += Resolver.url("sbt-android-plugin",
                          url("https://raw.github.com/dant3/sbt-android-plugin/repo/"))(Resolver.ivyStylePatterns)

addSbtPlugin("com.github.dant3" % "sbt-android-buildinfo" % "0.1")
