package paperboy.rss

import org.scalatest.{Matchers, FlatSpec}
import scala.None

class FeedReaderSpec extends FlatSpec with Matchers {
    "Feed Reader" should "Parse habrahabr feed correctly" in {
        val feed = Feed from getClass.getResource("habrafeed.xml")
        feed.channel.items.length should be > 0
    }

    "Feed Reader" should "Parse rss2feed correctly" in {
        val feed = Feed from getClass.getResource("rss2feed.xml")
        feed should be equals rss2feedSample
    }

    def rss2feedSample = {
        new Feed(
            new Channel(
                Some("Liftoff News"),
                Some("http://liftoff.msfc.nasa.gov/"),
                Some("Liftoff to Space Exploration."),
                Some("en-us"),
                Some("Tue, 10 Jun 2003 04:00:00 GMT"),
                Some("Tue, 10 Jun 2003 09:41:01 GMT"),
                Some("http://blogs.law.harvard.edu/tech/rss"),
                Some("Weblog Editor 2.0"),
                Some("editor@example.com"),
                Some("webmaster@example.com"),
                Seq.empty[String],
                None,
                Seq(
                    new Item(
                        Some("Star City"),
                        Some("http://liftoff.msfc.nasa.gov/news/2003/news-starcity.asp"),
                        Some("""How do Americans get ready to work with Russians aboard the
                               |                International Space Station? They take a crash course in culture, language
                               |                and protocol at Russia's Star City."""),
                        Some("Tue, 03 Jun 2003 09:39:21 GMT"),
                        Some("http://liftoff.msfc.nasa.gov/2003/06/03.html#item573"),
                        None,
                        None,
                        None,
                        Seq.empty[String]
                    ), new Item(
                        Some("Space Exploration"),
                        Some("http://liftoff.msfc.nasa.gov/"),
                        Some("""Sky watchers in Europe, Asia, and parts of Alaska and Canada
                               |                will experience a partial eclipse of the Sun on Saturday, May 31st."""),
                        Some("Fri, 30 May 2003 11:06:42 GMT"),
                        Some("http://liftoff.msfc.nasa.gov/2003/05/30.html#item572"),
                        None,
                        None,
                        None,
                        Seq.empty[String]
                    )
                )
            )
        )
    }
//
//    def json(login:String, password:String) =
//        s"""{
//          | "request_id" : "123qwe",
//          | "commands" : [{
//          |     "type" : "login",
//          |     "auth_type" : "plaintext",
//          |     "auth_data" : {
//          |         "login" : $login,
//          |         "password" : $password
//          |     }
//          | }]
//          | }
//        """.stripMargin
}
