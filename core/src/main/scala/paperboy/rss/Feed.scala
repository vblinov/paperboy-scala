package paperboy.rss

import java.io.InputStream
import java.net.URL
import scala.Some
import scala.xml._

class Feed(val channel: Channel)

object Feed {
    def from(is: InputStream): Feed = Feed.apply(XML load is)
    def from(source: InputSource): Feed = Feed.apply(XML load source)
    def from(url: URL): Feed = Feed.apply(XML load url)
    def from(xml: String): Feed = Feed.apply(XML loadString xml)

    def apply(n: Node):Feed = (n \ "channel").headOption match {
        case Some(node: Node) => new Feed(Channel(node))
        case _ => throw new IllegalArgumentException("Feed should have channel node")
    }
}