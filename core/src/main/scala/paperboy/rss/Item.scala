package paperboy.rss

import scala.xml.Node

class Item(val title: Option[String],
           val link: Option[String],
           val description: Option[String],
           val pubDate: Option[String],
           val guid: Option[String],
           val author: Option[String],
           val comments: Option[String],
           val source: Option[String],
           val categories: Seq[String]
)

object Item extends FeedReader {
    def apply(n: Node): Item = new Item(
        getText(n \ "title"),
        getText(n \ "link"),
        getText(n \ "description"),
        getText(n \ "pubDate"),
        getText(n \ "guid"),
        getText(n \ "author"),
        getText(n \ "comments"),
        getText(n \ "source"),
        (n \ "category").map( (n: Node) => n.text )
    )
}