package paperboy.rss

import scala.None
import scala.xml.{Node, NodeSeq}

trait FeedReader {
    def getHeadContent[T](n: NodeSeq, getContent:(Node) => T) = n.headOption match {
        case Some(node: Node) => Some(getContent(node))
        case _ => None
    }

    def getText(n: NodeSeq): Option[String] = getHeadContent(n, _.text)
}