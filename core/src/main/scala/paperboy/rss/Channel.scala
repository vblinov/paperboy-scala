package paperboy.rss

import scala.xml.Node

class Channel(val title: Option[String],
              val link: Option[String],
              val description: Option[String],
              val language: Option[String],
              val pubDate: Option[String],
              val lastBuildDate: Option[String],
              val docs: Option[String],
              val generator: Option[String],
              val managingEditor: Option[String],
              val webMaster: Option[String],
              val categories: Seq[String],
              val ttl: Option[String],
              val items: Seq[Item]
)

object Channel extends FeedReader {
    def apply(n: Node): Channel = new Channel(
        getText(n \ "title"),
        getText(n \ "link"),
        getText(n \ "description"),
        getText(n \ "language"),
        getText(n \ "pubDate"),
        getText(n \ "lastBuildDate"),
        getText(n \ "docs"),
        getText(n \ "generator"),
        getText(n \ "managingEditor"),
        getText(n \ "webMaster"),
        (n \ "category").map( (n: Node) => n.text ),
        getText(n \ "ttl"),
        (n \ "item").map( (n: Node) => Item(n) )
    )
}