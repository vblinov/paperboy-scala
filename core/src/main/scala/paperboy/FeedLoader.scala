package paperboy

import paperboy.rss.Feed
import scala.concurrent.Future

import dispatch._
import Defaults._


object FeedLoader {
    def getFeed(feedUrl: String): Future[Feed] = {
        val request = url(feedUrl)
        Http(request OK as.String).map(Feed.from)
    }
}
