/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package paperboy.android

import android.content.Context
import android.database.Cursor
import android.view.{ViewGroup, View}
import android.widget.CursorAdapter
import paperboy.android.data.{FeedTable, AllFeeds}

class FeedsAdapter(context:Context) extends CursorAdapter(context, FeedsAdapter.queryAllFeeds(context), true) {
    override def newView(context: Context, cursor: Cursor, parent: ViewGroup): View = new FeedView(context)
    override def bindView(convertView: View, context: Context, cursor: Cursor): Unit = Option(convertView) match {
        case Some(feedView: FeedView) => feedView.bind(FeedTable.Item(cursor))
    }
}

object FeedsAdapter {
    def queryAllFeeds(context:Context):Cursor = {
        context.getContentResolver.
            query(AllFeeds, FeedTable.projection, null, null, FeedTable.sortOrder)
    }
}
