/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package paperboy.android.data

import android.content.Context
import android.database.sqlite.{SQLiteDatabase, SQLiteOpenHelper}
import android.net.Uri

class DBHelper(context: Context) extends SQLiteOpenHelper(context, DBHelper.databaseName, null, DBHelper.databaseVer) {
    override def onCreate(database: SQLiteDatabase): Unit = {
        for (table <- DBHelper.tables) {
            table.create(database)
        }

        // populate initial data:
        FeedTable.Item("Habrahabr / Scala", Uri.parse("http://habrahabr.ru/rss/hub/scala/")).insert(database)
    }

    override def onUpgrade(database: SQLiteDatabase, oldVer: Int, newVer: Int): Unit = {
        for (table <- DBHelper.tables) {
            table.maybeUpgrade(database, oldVer, newVer)
        }
    }
}

object DBHelper {
    lazy val databaseName = "paperboy"
    lazy val databaseVer  = tables.map(_.tableVersion).max

    lazy val tables:Seq[DatabaseTable] = Seq(FeedTable, FeedItemTable)
}
