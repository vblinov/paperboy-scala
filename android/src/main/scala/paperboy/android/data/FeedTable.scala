/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package paperboy.android.data

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import org.joda.time.DateTime
import paperboy.rss.Feed

object FeedTable extends DatabaseTable {
    override val tableName = "feed"
    override val tableVersion = 1


    override def create(db: SQLiteDatabase): Any = {
        db.execSQL(
            s"""CREATE TABLE $tableName (
               |$ID INTEGER PRIMARY KEY,
               |$name TEXT,
               |$address TEXT,
               |$icon TEXT,
               |$lastUpdated INTEGER
               |);
             """.stripMargin
        )

        db.execSQL(s"CREATE INDEX lastUpdatedIndex1 ON $tableName ($lastUpdated);")
    }

    override protected def upgrade(db: SQLiteDatabase, oldVer: Int, newVer: Int): Any = ???


    val ID = "_id"
    val name = "name"
    val address = "address"
    val icon = "icon"
    val lastUpdated = "lastUpdated"

    lazy val projection:Array[String] = Array(ID, name, address, icon, lastUpdated)
    val sortOrder = s"$lastUpdated ASC"

    case class Item(name: String, address: Uri, icon: Option[Uri] = None, lastUpdated: Option[DateTime] = None,
                    id: Option[Long] = None) {
        def insert(db: SQLiteDatabase) = db.insert(FeedTable.tableName, null, toContentValues)

        def toContentValues = {
            val values = new ContentValues()
            for (myID <- id) values.put(FeedTable.ID, myID.asInstanceOf[java.lang.Long])
            values.put(FeedTable.name, name)
            values.put(FeedTable.address, address.toString)
            for (iconUri <- icon) values.put(FeedTable.icon, iconUri.toString)
            for (lastUpdate <- lastUpdated) values.put(FeedTable.lastUpdated, lastUpdate.getMillis.asInstanceOf[java.lang.Long])
            values
        }
    }


    object Item {
        import paperboy.android.data.cursor.RichCursor

        def apply(cursor:Cursor): Item = apply(cursor:RichCursor)

        def apply(cursor: RichCursor): Item = new Item(
            name = cursor.getString(FeedTable.name),
            address = cursor.getUri(FeedTable.address),
            icon = cursor.getUriOption(FeedTable.icon),
            lastUpdated = cursor.getLongOption(FeedTable.lastUpdated).map(new DateTime(_)),
            id = cursor.getLongOption(FeedTable.ID)
        )

        def apply(feed: Feed, feedUri: Uri): Item = new Item(
            name = feed.channel.title.getOrElse("<no title>"),
            address = feedUri
        )
    }
}
