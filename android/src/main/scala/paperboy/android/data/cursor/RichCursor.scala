/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package paperboy.android.data.cursor

import android.database.Cursor
import android.net.Uri

class RichCursor(val originalCursor: Cursor) extends CursorProxy {
    def getBlob(column: String):Array[Byte] = getBlob(getColumnIndex(column))
    def getDouble(column: String):Double = getDouble(getColumnIndex(column))
    def getFloat(column: String):Float = getFloat(getColumnIndex(column))
    def getInt(column: String):Int = getInt(getColumnIndex(column))
    def getLong(column: String):Long = getLong(getColumnIndex(column))
    def getShort(column: String):Short = getShort(getColumnIndex(column))
    def getString(column: String):String = getString(getColumnIndex(column))
    def getUri(column: String): Uri = Uri.parse(getString(getColumnIndex(column)))

    def getBlobOption(column: String):Option[Array[Byte]] = Option(getBlob(column))
    def getDoubleOption(column: String):Option[Double] = Option(getDouble(column))
    def getFloatOption(column: String):Option[Float] = Option(getFloat(column))
    def getIntOption(column: String):Option[Int] = Option(getInt(column))
    def getLongOption(column: String):Option[Long] = Option(getLong(column))
    def getShortOption(column: String):Option[Short] = Option(getShort(column))
    def getStringOption(column: String): Option[String] = Option(getString(column))
    def getUriOption(column: String): Option[Uri] = getStringOption(column).map(Uri.parse)
}

object RichCursor {
    implicit def apply(cursor: Cursor): RichCursor = new RichCursor(cursor)
    def unapply(cursor:RichCursor): Cursor = cursor.originalCursor
}