/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package paperboy.android.data.cursor

import android.content.ContentResolver
import android.database.{CharArrayBuffer, ContentObserver, Cursor, DataSetObserver}
import android.net.Uri
import android.os.Bundle

trait CursorProxy extends Cursor {
    def originalCursor: Cursor

    override def getCount: Int = originalCursor.getCount
    override def moveToFirst(): Boolean = originalCursor.moveToFirst()
    override def getType(p1: Int): Int = originalCursor.getType(p1)
    override def isBeforeFirst: Boolean = originalCursor.isBeforeFirst
    override def getPosition: Int = originalCursor.getPosition
    override def move(p1: Int): Boolean = originalCursor.move(p1)
    override def registerContentObserver(p1: ContentObserver): Unit = originalCursor.registerContentObserver(p1)
    override def getExtras: Bundle = originalCursor.getExtras
    override def moveToNext(): Boolean = originalCursor.moveToNext()
    override def isAfterLast: Boolean = originalCursor.isAfterLast
    override def getWantsAllOnMoveCalls: Boolean = originalCursor.getWantsAllOnMoveCalls
    override def getColumnIndex(p1: String): Int = originalCursor.getColumnIndex(p1)
    override def moveToPrevious(): Boolean = originalCursor.moveToPrevious()
    override def isLast: Boolean = originalCursor.isLast
    override def getDouble(p1: Int): Double = originalCursor.getDouble(p1)
    override def unregisterContentObserver(p1: ContentObserver): Unit = originalCursor.unregisterContentObserver(p1)
    override def isFirst: Boolean = originalCursor.isFirst
    override def getColumnIndexOrThrow(p1: String): Int = originalCursor.getColumnIndexOrThrow(p1)
    override def moveToLast(): Boolean = originalCursor.moveToLast()
    override def getColumnCount: Int = originalCursor.getColumnCount
    override def getColumnName(p1: Int): String = originalCursor.getColumnName(p1)
    override def getFloat(p1: Int): Float = originalCursor.getFloat(p1)
    override def registerDataSetObserver(p1: DataSetObserver): Unit = originalCursor.registerDataSetObserver(p1)
    override def getLong(p1: Int): Long = originalCursor.getLong(p1)
    override def copyStringToBuffer(p1: Int, p2: CharArrayBuffer): Unit = originalCursor.copyStringToBuffer(p1, p2)
    override def moveToPosition(p1: Int): Boolean = originalCursor.moveToPosition(p1)
    override def setNotificationUri(p1: ContentResolver, p2: Uri): Unit = originalCursor.setNotificationUri(p1, p2)
    override def getNotificationUri:Uri = originalCursor.getNotificationUri
    override def unregisterDataSetObserver(p1: DataSetObserver): Unit = originalCursor.unregisterDataSetObserver(p1)
    override def getShort(p1: Int): Short = originalCursor.getShort(p1)
    override def isNull(p1: Int): Boolean = originalCursor.isNull(p1)
    override def respond(p1: Bundle): Bundle = originalCursor.respond(p1)
    override def close(): Unit = originalCursor.close()
    override def isClosed: Boolean = originalCursor.isClosed
    override def getColumnNames: Array[String] = originalCursor.getColumnNames
    override def getInt(p1: Int): Int = originalCursor.getInt(p1)
    override def getBlob(p1: Int): Array[Byte] = originalCursor.getBlob(p1)
    override def getString(p1: Int): String = originalCursor.getString(p1)

    @deprecated override def deactivate(): Unit = originalCursor.deactivate()
    @deprecated override def requery(): Boolean = originalCursor.requery()
}

