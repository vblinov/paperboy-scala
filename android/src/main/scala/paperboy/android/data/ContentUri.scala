/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package paperboy.android.data

import android.content.UriMatcher
import android.net.Uri

sealed trait ContentUriContract {
    def authority: String = RSSFeedsContentProvider.Contract.AUTHORITY
    def basePath: String
    implicit def toUri: Uri = Uri.parse(s"content://$authority/$basePath")
    def contentType: String
}

sealed trait ContentUri extends ContentUriContract {
    def path: String = basePath
    override implicit def toUri: Uri = Uri.parse(s"content://$authority/$path")
}

object ContentUri {
    val CONTENT_TYPE_BASE = s"vnd.${RSSFeedsContentProvider.Contract.AUTHORITY}"
    val CURSOR_DIR = "vnd.android.cursor.dir"
    val CURSOR_ITEM = "vnd.android.cursor.item"

    val contracts = Vector(AllFeeds, SingleFeed)

    private val matcher: UriMatcher = {
        val result = new UriMatcher(UriMatcher.NO_MATCH)
        for (contract <- contracts) result.addURI(contract.authority, contract.basePath, contracts.indexOf(contract))
        result
    }

    def apply(uri: Uri): ContentUri = {
        val matchIndex = matcher.`match`(uri)
        contracts.lift(matchIndex) match {
            case Some(AllFeeds) => AllFeeds
            case Some(SingleFeed) => SingleFeed(uri.getLastPathSegment.toInt)
        }
    }

    implicit def toUri(contentUri: ContentUriContract) = contentUri.toUri
}

object AllFeeds extends ContentUriContract with ContentUri {
    override def basePath: String = "feeds"
    override def contentType: String = ContentUri.CURSOR_DIR + "/" + ContentUri.CONTENT_TYPE_BASE + ".feed"
}

case class SingleFeed(feedID: Long) extends ContentUri {
    override def basePath: String = SingleFeed.basePath + "/#"
    override def path = basePath + feedID
    override def contentType: String = SingleFeed.contentType
}

object SingleFeed extends ContentUriContract {
    override def basePath: String = "feed" + "/#"
    override def contentType: String = ContentUri.CURSOR_ITEM + "/" + ContentUri.CONTENT_TYPE_BASE + ".feed"
}