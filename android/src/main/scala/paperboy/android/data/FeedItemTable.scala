/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package paperboy.android.data

import android.database.sqlite.SQLiteDatabase

object FeedItemTable extends DatabaseTable {
    override val tableName = "feed_item"
    override def tableVersion: Int = 1


    override def create(db: SQLiteDatabase): Any = {
        db.execSQL(
            s"""CREATE TABLE $tableName (
               |$ID INTEGER PRIMARY KEY,
               |$title TEXT,
               |$text TEXT,
               |$link TEXT,
               |$icon TEXT,
               |$added INTEGER
               |);
             """.stripMargin
        )
        db.execSQL(s"CREATE INDEX addedIndex1 ON $tableName ($added);")
    }

    override protected def upgrade(db: SQLiteDatabase, oldVer: Int, newVer: Int): Any = ???


    val ID = "_id"
    val title = "title"
    val text = "text"
    val link = "link"
    val icon = "icon"
    val added = "added"

}
