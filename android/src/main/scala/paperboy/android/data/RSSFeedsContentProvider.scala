/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package paperboy.android.data

import android.content.{ContentProvider, ContentValues, UriMatcher}
import android.database.Cursor
import android.net.Uri
import android.text.TextUtils
import paperboy.android.data.RSSFeedsContentProvider.Contract

class RSSFeedsContentProvider extends ContentProvider {
    lazy val dbSource = new DBHelper(getContext)

    override def onCreate(): Boolean = dbSource != null // can we do better?

    override def getType(uri: Uri): String = ContentUri(uri).contentType

    override def update(uri: Uri, values: ContentValues, selection: String, selectionArgs: Array[String]): Int = {
        val result = ContentUri(uri) match {
            case AllFeeds => dbSource.getWritableDatabase.update(FeedTable.tableName, values, selection, selectionArgs)
            case SingleFeed(feed) => dbSource.getWritableDatabase.update(
                FeedTable.tableName, values, selectionWith(selection, FeedTable.ID + " = " + feed), selectionArgs)
        }
        getContext.getContentResolver.notifyChange(uri, null)
        result
    }

    override def insert(uri: Uri, values: ContentValues): Uri = {
        val feedID = ContentUri(uri) match {
            case AllFeeds => dbSource.getWritableDatabase.insert(FeedTable.tableName, null, values)
        }
        val resultUri = SingleFeed(feedID)
        getContext.getContentResolver.notifyChange(resultUri, null)
        resultUri
    }

    override def delete(uri: Uri, selection: String, selectionArgs: Array[String]): Int = {
        // android+scala way
        val deletedCount = ContentUri(uri) match {
            case AllFeeds => dbSource.getWritableDatabase.delete(FeedTable.tableName, selection, selectionArgs)
            case SingleFeed(feedID) => dbSource.getWritableDatabase.delete(
                FeedTable.tableName, selectionWith(selection, FeedTable.ID + " = " + feedID), selectionArgs)
        }
        getContext.getContentResolver.notifyChange(uri, null)
        deletedCount
    }

    override def query(uri: Uri, projection: Array[String], selection: String, selectionArgs: Array[String], sortOrder: String): Cursor = {
        // android+java way
        RSSFeedsContentProvider.uriMatcher.`match`(uri) match {
            case Contract.FEEDS_PATH_ID =>
                val sorting = if (TextUtils.isEmpty(sortOrder)) FeedTable.lastUpdated + " DESC" else sortOrder
                query(FeedTable.tableName, projection, selection, selectionArgs, sorting)
            case Contract.FEED_PATH_ID =>
                val feedID = uri.getLastPathSegment
                val selectionScoped = selectionWith(selection, FeedTable.ID + " = " + feedID)
                query(FeedTable.tableName, projection, selectionScoped, selectionArgs, sortOrder)
        }
    }


    private def query (table: String, projection: Array[String], selection: String, selectionArgs: Array[String], sortOrder: String) = {
        val cursor = dbSource.getWritableDatabase.query(table, projection, selection, selectionArgs, null, null, sortOrder)
        cursor.setNotificationUri(getContext.getContentResolver, Contract.FEEDS_CONTENT_URI)
        cursor
    }

    private def selectionWith(selection: String, additionalParam: String): String = if (TextUtils.isEmpty(selection.trim)) {
        additionalParam
    } else {
        selection + " AND " + additionalParam
    }
}

object RSSFeedsContentProvider {
    private val uriMatcher = {
        val result = new UriMatcher(UriMatcher.NO_MATCH)
        result.addURI(Contract.AUTHORITY, Contract.FEEDS_PATH, Contract.FEEDS_PATH_ID)
        result.addURI(Contract.AUTHORITY, Contract.FEED_PATH, Contract.FEED_PATH_ID)
        result
    }

    object Contract {
        val AUTHORITY = "com.github.dant3.paperboy"
        val FEEDS_PATH = "feeds"
        val FEEDS_PATH_ID = 1
        val FEED_PATH = "feed/#"
        val FEED_PATH_ID = 2

        val FEEDS_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + FEEDS_PATH)
    }
}