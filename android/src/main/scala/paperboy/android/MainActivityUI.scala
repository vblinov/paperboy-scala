/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package paperboy.android

import java.util.concurrent.CancellationException

import android.app.Activity
import android.net.Uri
import android.os.Bundle
import android.widget.{EditText, ListAdapter}
import com.github.dant3.paperboy.{R, TR, TypedActivity}
import org.scaloid.common._
import paperboy.android.utils.PaperboyActivity

import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success, Try}

trait MainActivityUI extends Activity with SActivity with PaperboyActivity with TypedActivity {

    override def onCreate(savedInstanceState: Bundle) = {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        val listView = findView(TR.content)
        listView.setAdapter(contentAdapter)
        val fab = findView(TR.fab)
        fab.setOnClickListener(addFeed(askFeedUri))
    }


    def contentAdapter:ListAdapter

    def addFeed(feedUri: Future[Uri]): Unit


    def askFeedUri: Future[Uri] = {
        val promise = Promise[Uri]()
        val textEditor:EditText = new EditText(this)
        textEditor.setHint("Enter feed URI")

        def acceptInput():Unit = Try(Uri.parse(textEditor.getText.toString)) match {
            case Success(uri: Uri) =>
                promise.success(uri)
            case Failure(_) =>
                longToast("Incorrect Uri input")
        }

        def cancelTask(): Unit = promise.failure(new CancellationException())

        new AlertDialogBuilder("Add Feed") {
            setView(textEditor)
            positiveButton("Add", acceptInput())
            negativeButton(android.R.string.cancel, cancelTask())
        }.show()
        promise.future
    }
}
