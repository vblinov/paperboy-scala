/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package paperboy.android

import android.app.Activity
import android.net.Uri
import android.widget.ListAdapter
import macroid.Contexts
import macroid.FullDsl._
import paperboy.FeedLoader
import paperboy.android.data.{AllFeeds, FeedTable}
import paperboy.rss.Feed

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class MainActivity extends Activity with MainActivityUI with Contexts[MainActivity] {
    lazy val contentAdapter: ListAdapter = new FeedsAdapter(this)

    val scalaVersion = getString(com.github.dant3.paperboy.R.string.scalaVersion)

    override def addFeed(feedUri: Future[Uri]): Unit = {
        feedUri.map { uri =>
            val downloadFeed = FeedLoader.getFeed(uri.toString)
            downloadFeed.onSuccessUi {
                case feed: Feed =>
                    val item = FeedTable.Item(feed, uri)
                    getContentResolver.insert(
                        AllFeeds, item.toContentValues
                    )
            }

            downloadFeed.onFailureUi {
                case error: Throwable =>
                    logD"Got error $error"("======>")
                    dialog(s"Got $error") <~ title("Error!") <~ speak
            }
        }
    }
}
