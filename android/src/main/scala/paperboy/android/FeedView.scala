/**
 * Copyright 2014 dant3
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package paperboy.android

import android.content.Context
import android.view.View
import android.widget.RelativeLayout
import com.github.dant3.paperboy.{R, TR, TypedView}
import com.squareup.picasso.Picasso
import paperboy.android.data.FeedTable

class FeedView(context: Context) extends RelativeLayout(context) with TypedView with DataView[FeedTable.Item] {
    View.inflate(context, R.layout.view_feed, this)

    lazy val titleView = findView(TR.feed_title)
    lazy val iconView = findView(TR.feed_icon)


    override def bind(item: FeedTable.Item):Unit = {
        titleView.setText(item.name)
        for (icon <- item.icon) Picasso.`with`(context).load(icon).fit().into(iconView)
    }
}
